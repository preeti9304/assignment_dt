import sys
from bs4 import BeautifulSoup
import requests
import urllib.parse
import threading
from utils import base_url,second_url,delay_time
import time
import logging
from log_module import logger

class Scrapper(object):
    def __init__(self):
        """
        @current_temperature_index: initializing the temperature index
        the temperature in the response object occurs at the 4th index
        """
        self.current_temperature_index=4

    def ajax_scrapper_every_five_second(self):
        """
        this method gives the temperature in every five seconds .
        @res: res from the ajax_scrapper method
        @timer: obj of the Timer class
        :return:No return value
        """

        try:
            res = self.ajax_scrapper()
            timer = threading.Timer(delay_time, self.ajax_scrapper_every_five_second)
            if(res[0]):
                timer.start()
                logger.info(res[1])
            else:
                timer.cancel()
                logger.info(res[1])
        except Exception:
            logger.info("Server not Available");


    def ajax_scrapper(self):
        """
         method that does a GET request
         against weerindelft, checks the temperature of the
         city and returns the temperature
         @current_time: current time
         @request_url: url for the get request
         @res: response received from the get request
         @temperature: current temperature in delft
         :return: It returns the tuple of a flag and custom message
         If flag=1 -> success
            flag=0 -> failure
        """
        try:
            current_time = self.current_time_milliseconds()
            request_url = urllib.parse.urljoin(base_url, second_url) + "?" + str(current_time)
            res = requests.get(request_url)
            if (res.status_code == 200):
                res = res.text
                res_list = res.split(' ')
                temperature = str(round(float(res_list[self.current_temperature_index]))) + " degree celcius"
                return (1,temperature)
            elif (res.status_code == 404):
                return(0,"Resource Not Found")
            else:
                return (0, "Server Not Available")
        except TypeError as t:
            return(1,"Type error occured")
        except ValueError as i:
            return(1,"Value error occurred")
        except AttributeError as a:
            return(1,"Attribute error occurred")
        except IndexError as ie:
            return(1,"Index out of range")
        except Exception as e:
            return (1,"Server Not Available")

    def scrapperWrapper(self):
        """
        this method is the wrapper over ajax_scrapper.
        It gets the return response from ajax_scrapper
        and show it to user
        @res: response from ajax_scrapper method
        """
        logger.info("start")
        res = self.ajax_scrapper()
        logger.info(res[1])

    def current_time_milliseconds(self):
        return int(time.time() * 1000)


if __name__ == '__main__':
    sc = Scrapper()
    if(sys.argv[1] == "periodic"):
        sc.ajax_scrapper_every_five_second()
    elif(sys.argv[1] == "once"):
        sc.scrapperWrapper()
