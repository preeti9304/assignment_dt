FROM python:3.7-alpine

Add . /webscrapper
WORKDIR /webscrapper
RUN echo "$PWD"
ENV arg arg_value
RUN pip install -r requirements.txt

CMD python scraper.py ${arg}